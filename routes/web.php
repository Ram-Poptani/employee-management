<?php

use App\Http\Controllers\EmployeeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(route('employee.index'));
});

Route::get('index', 'App\Http\Controllers\EmployeeController@index')->name('employee.index');
Route::get('create', 'App\Http\Controllers\EmployeeController@create')->name('employee.create');
Route::post('store', 'App\Http\Controllers\EmployeeController@store')->name('employee.store');
Route::post('{employee}/destroy', 'App\Http\Controllers\EmployeeController@destroy')->name('employee.destroy');
Route::get('edit/{employee}', 'App\Http\Controllers\EmployeeController@edit')->name('employee.edit');
Route::put('update/{employee}', 'App\Http\Controllers\EmployeeController@update')->name('employee.update');

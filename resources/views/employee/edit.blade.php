{{-- {{ dd($employee->phones[0]->phone) }} --}}
<!doctype html>
<html lang="en">
  <head>
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!-- FONTAWESOME -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  </head>
  <body>
    
    <div class="container">
        <a class="btn btn-primary" href="{{ route('employee.index') }}" role="button">HOME <i class="fa fa-home" aria-hidden="true"></i></a>
        <div class="row">
            <div class="col-md-12">

                <div class="card">
                    <div class="card-header">
                        Employee Details
                    </div>
                    <div class="card-body">
                        <form action="{{ route('employee.update', $employee) }}" method="POST" id="employeeForm">
                            @method('PUT')
                            @csrf
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text"
                                    class="form-control" name="name" id="name" value="{{ old('name', $employee->name) }}" placeholder="Enter Name">
                                @error('name')
                                    <div class="text-danger">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
        
                            <div class="card" id="addressCard">
                                <div class="card-header d-flex justify-content-between">
                                    <p>Employee Address Details</p>
                                    <button id="addAddress" type="button" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                </div>
                                @foreach ($employee->addresses as $address)
                                    
                                
                                <div class="card-body row">

                                    @if ($address != $employee->addresses[0])
                                    <div class="col-md-12 d-flex flex-row-reverse">
                                        <button type="button" class="btn btn-danger" onclick="this.parentNode.parentNode.remove()"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                    </div>    
                                    @endif
                                    

                                    <div class="form-group col-md-6">
                                      <label for="address[]">Address </label>
                                      <input type="text"
                                        class="form-control" name="address[]" value="{{ old('address', $address->address) }}" id="address[]" placeholder="Enter Address">
                                    </div>
        

                                    <div class="form-group col-md-6">
                                        <label for="zip[]">zip </label>
                                        <input type="text"
                                          class="form-control" name="zip[]" id="zip[]"  value="{{ old('zip', $address->zip) }}"placeholder="Enter zip">
                                    </div>
        
        
                                    <div class="form-group col-md-6">
                                        <label for="suburb[]">suburb </label>
                                        <input type="text"
                                          class="form-control" name="suburb[]" id="suburb[]"  value="{{ old('suburb', $address->suburb) }}"
                                          placeholder="Enter suburb">
                                    </div>
        

                                    <div class="form-group col-md-6">
                                        <label for="location[]">location </label>
                                        <input type="text"
                                          class="form-control" name="location[]" id="location[]"  value="{{ old('location', $address->location) }}" placeholder="Enter location">
                                    </div>

                                    
                                    <div class="form-group col-md-6">
                                        <label for="taluka[]">taluka </label>
                                        <input type="text"
                                          class="form-control" name="taluka[]" id="taluka[]"  value="{{ old('taluka', $address->taluka) }}" placeholder="Enter taluka">
                                    </div>
        
        
                                    <div class="form-group col-md-6">
                                        <label for="city[]">city </label>
                                        <input type="text"
                                          class="form-control" name="city[]" id="city[]"  value="{{ old('city', $address->city) }}" placeholder="Enter city">
                                    </div>
        
        
                                    <div class="form-group col-md-6">
                                        <label for="district[]">district </label>
                                        <input type="text"
                                          class="form-control" name="district[]" id="district[]"  value="{{ old('district', $address->district) }}" placeholder="Enter district">
                                    </div>
        

                                    <div class="form-group col-md-6">
                                        <label for="state[]">state </label>
                                        <input type="text"
                                          class="form-control" name="state[]" id="state[]"  value="{{ old('state', $address->state) }}" placeholder="Enter state">
                                    </div>
        
                                    <div class="form-group col-md-6">
                                        <label for="country[]">country </label>
                                        <input type="text"
                                          class="form-control" name="country[]" id="country[]"  value="{{ old('country', $address->country) }}" placeholder="Enter country">
                                    </div>
                                </div>

                                @endforeach
                            </div>



                            <div class="card" id="contactCard">
                                <div class="col-md-12 card-header d-flex justify-content-between">
                                    <p>Employee Contact Details</p>
                                    <button id="addContact" type="button" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                </div>
                                @foreach ($employee->phones as $phone)
                                    
                                
                                <div class="card-body">
                                    @if ($phone != $employee->phones[0])
                                    <div class="col-md-12 d-flex flex-row-reverse">
                                        <button type="button" class="btn btn-danger" onclick="this.parentNode.parentNode.remove()"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                    </div>    
                                    @endif
                                    
                                    <div class="form-group">
                                      <label for="phone[]">Phone</label>
                                      <input type="text"
                                        class="form-control" name="phone[]" value="{{ old('phone', $phone->phone) }}" id="phone[]"  placeholder="Enter Phone number">
                                    </div>

                                    <div class="form-check ">
                                        <label class="form-check-label">
                                        <input type="radio" {{ $phone->isPrimary ? 'checked' : ''}} class="form-check-input" name="isPrimary" id="isPrimary">
                                        Primary
                                      </label>
                                    </div>
                                </div>

                                @endforeach
                            </div>


                            <div class="card" id="whatsappCard">
                                <div class="col-md-12 card-header d-flex justify-content-between">
                                    <p>Employee Whatsapp Contact Details</p>
                                    <button id="addWhatsappContact" type="button" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                </div>
                                @foreach ($employee->whatsappPhones as $whatsappPhone)
                                
                                
                                <div class="card-body">
                                    @if ($whatsappPhone != $employee->whatsappPhones[0])
                                    <div class="col-md-12 d-flex flex-row-reverse">
                                        <button type="button" class="btn btn-danger" onclick="this.parentNode.parentNode.remove()"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                    </div>    
                                    @endif
                                    <div class="form-group">
                                      <label for="whatsappPhone[]">Phone</label>
                                      <input type="text"
                                        class="form-control" name="whatsappPhone[]" id="whatsappPhone[]" value="{{ old('whatsappPhone', $whatsappPhone->phone) }}" placeholder="Enter Whatsapp Phone number">
                                    </div>

                                    <div class="form-check ">
                                        <label class="form-check-label">
                                        <input type="radio" class="form-check-input" name="isPrimaryWhatsapp" {{ $whatsappPhone->isPrimary ? 'checked' : ''}} id="isPrimaryWhatsapp">
                                        Whatsapp Primary
                                      </label>
                                    </div>
                                </div>
                                @endforeach
                            </div>


                            <div class="card" id="emailCard">
                                <div class="col-md-12 card-header d-flex justify-content-between">
                                    <p>Employee Email Details</p>
                                    <button id="addEmail" type="button" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                </div>
                                @foreach ($employee->emails as $email)
                                <div class="card-body">
                                    @if ($email != $employee->emails[0])
                                    <div class="col-md-12 d-flex flex-row-reverse">
                                        <button type="button" class="btn btn-danger" onclick="this.parentNode.parentNode.remove()"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                    </div>    
                                    @endif
                                    <div class="form-group">
                                      <label for="email[]">Email</label>
                                      <input type="email"
                                        class="form-control" name="email[]" id="email[]"  value="{{ $email->email }}" placeholder="Enter email id">
                                    </div>
                                </div>
                                @endforeach
                            </div>

                            <div class="form-group p-5 text-center">
                                <button type="submit" class="btn btn-primary">Update Employee Details</button>
                            </div>
                        </form>
                    </div>
                </div>

                
            </div>
        </div>  
    </div>
    

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>








    <!-- CUSTOM JS -->
    <script>


$('#addAddress').on('click', e => {
            let addressCard = $('#addressCard');
            let addressCardFields = `<div class="card-body row">
                                        <div class="col-md-12 d-flex flex-row-reverse">
                                            <button type="button" class="btn btn-danger" onclick="this.parentNode.parentNode.remove()"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                        </div>    
                                        <div class="form-group col-md-6">
                                        <label for="address[]">Address </label>
                                        <input type="text"
                                            class="form-control" name="address[]" id="address[]" placeholder="Enter Address">
                                        </div>
            

                                        <div class="form-group col-md-6">
                                            <label for="zip[]">zip </label>
                                            <input type="text"
                                            class="form-control" name="zip[]" id="zip[]" placeholder="Enter zip">
                                        </div>
            
            
                                        <div class="form-group col-md-6">
                                            <label for="suburb[]">suburb </label>
                                            <input type="text"
                                            class="form-control" name="suburb[]" id="suburb[]" placeholder="Enter suburb">
                                        </div>
            

                                        <div class="form-group col-md-6">
                                            <label for="location[]">location </label>
                                            <input type="text"
                                            class="form-control" name="location[]" id="location[]" placeholder="Enter location">
                                        </div>

                                        
                                        <div class="form-group col-md-6">
                                            <label for="taluka[]">taluka </label>
                                            <input type="text"
                                            class="form-control" name="taluka[]" id="taluka[]" placeholder="Enter taluka">
                                        </div>
            
            
                                        <div class="form-group col-md-6">
                                            <label for="city[]">city </label>
                                            <input type="text"
                                            class="form-control" name="city[]" id="city[]" placeholder="Enter city">
                                        </div>
            
            
                                        <div class="form-group col-md-6">
                                            <label for="district[]">district </label>
                                            <input type="text"
                                            class="form-control" name="district[]" id="district[]" placeholder="Enter district">
                                        </div>
            

                                        <div class="form-group col-md-6">
                                            <label for="state[]">state </label>
                                            <input type="text"
                                            class="form-control" name="state[]" id="state[]" placeholder="Enter state">
                                        </div>
            
                                        <div class="form-group col-md-6">
                                            <label for="country[]">country </label>
                                            <input type="text"
                                            class="form-control" name="country[]" id="country[]" placeholder="Enter country">
                                        </div>
                                    </div>`;
            addressCard.append(addressCardFields);
        });


        $('#addContact').on('click', e => {
            let contactCard = $('#contactCard');
            let contactCardFields = `<div class="card-body">
                                        <div class="col-md-12 d-flex flex-row-reverse">
                                            <button type="button" class="btn btn-danger" onclick="this.parentNode.parentNode.remove()"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                        </div>    
                                        <div class="form-group">
                                        <label for="phone[]">Phone</label>
                                        <input type="text"
                                            class="form-control" name="phone[]" id="phone[]"  placeholder="Enter Phone number">
                                        </div>

                                        <div class="form-check ">
                                            <label class="form-check-label">
                                            <input type="radio" class="form-check-input" name="isPrimary" id="isPrimary">
                                            Primary
                                        </label>
                                        </div>
                                    </div>`;
            contactCard.append(contactCardFields);
        });


        $('#addEmail').on('click', e => {
            let emailCard = $('#emailCard');
            let emailCardFields = `<div class="card-body">
                                        <div class="col-md-12 d-flex flex-row-reverse">
                                            <button type="button" class="btn btn-danger" onclick="this.parentNode.parentNode.remove()"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                        </div>    
                                        <div class="form-group">
                                        <label for="email[]">Email</label>
                                        <input type="email"
                                            class="form-control" name="email[]" id="email[]"  placeholder="Enter email id">
                                        </div>
                                    </div>`;
            emailCard.append(emailCardFields);
        });

        $('#addWhatsappContact').on('click', e => {
            let whatsappCard = $('#whatsappCard');
            let whatsappCardFields = `<div class="card-body">
                                        <div class="col-md-12 d-flex flex-row-reverse">
                                            <button type="button" class="btn btn-danger" onclick="this.parentNode.parentNode.remove()"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                        </div>    
                                        <div class="form-group">
                                        <label for="whatsappPhone[]">Phone</label>
                                        <input type="text"
                                            class="form-control" name="whatsappPhone[]" id="whatsappPhone[]"  placeholder="Enter Whatsapp Phone number">
                                        </div>

                                        <div class="form-check ">
                                            <label class="form-check-label">
                                            <input type="radio" class="form-check-input" name="isPrimaryWhatsapp" id="isPrimaryWhatsapp">
                                            Whatsapp Primary
                                        </label>
                                        </div>
                                    </div>`;
            whatsappCard.append(whatsappCardFields);
        });



        $('#employeeForm').submit(function(){


            $("input[name='isPrimary']").val($("input[name='isPrimary']:checked").parent().parent().prev().children("input[type='number']").val());

            $("input[name='isPrimaryWhatsapp']").val($("input[name='isPrimaryWhatsapp']:checked").parent().parent().prev().children("input[type='number']").val());
        });

        $('.btn-delete-panel').on('click', e => {
        });

    </script>




  </body>
</html>

<!doctype html>
<html lang="en">
  <head>
    <title>Employee Management</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!-- FONTAWESOME -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  </head>
  <body>

    <div class="container text-center">
        <h1>Employee Details</h1>
    </div>
    

    <div class="container d-flex justify-content-between">
        <div class=""></div>
        <a name="" id="" class="btn btn-primary" href="{{ route('employee.create') }}" role="button"><i class="fa fa-plus" aria-hidden="true"></i> Add</a>
    </div>

    <div class="container">
        @foreach ($employees as $employee)
        <div class="row d-block">
            <div class="card d-flex">
                <div class="card-header d-flex justify-content-between">
                    <h4 class="card-title">Name : {{ $employee->name }}</h4>
                    <!-- Button trigger modal -->
                    <div class="actions d-flex justify-content-around">
                        <form action="{{ route('employee.edit', $employee) }}" method="GET">
                            <button type="submit" class="btn btn-outline-success">Edit</button>
                        </form>
                        <button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#exampleModal">
                            <i class="fa fa-trash" aria-hidden="true"></i>
                        </button>
                    </div>
                    
                    
                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Are you sure you want to delete?</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            </div>
                            <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <form action="{{ route('employee.destroy', $employee) }}" class="" method="post">
                                    @method('POST')
                                    @csrf
                                    <button type="submit" class="btn btn-danger">Delete <i class="fa fa-trash" aria-hidden="true"></i></button>
                                </form>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    
                    <div class="card-text">
                        
                        <div class="row">
                            <div class="card d-block">
                                <div class="card-header">
                                    Adresses
                                </div>
                                @foreach ($employee->addresses as $adress)
                                <div class="card-body">
                                    <p>Address : {{ $adress->address }}</p>
                                    <p>Zip : {{ $adress->zip }}</p>
                                    <p>Suburb : {{ $adress->suburb }}</p>
                                    <p>Location : {{ $adress->location }}</p>
                                    <p>Taluka : {{ $adress->taluka }}</p>
                                    <p>City : {{ $adress->city }}</p>
                                    <p>District : {{ $adress->district }}</p>
                                    <p>State : {{ $adress->state }}</p>
                                    <p>Country : {{ $adress->country }}</p>
                                </div>    
                                @endforeach
                            </div>
                        </div>
        
                        <div class="row">
                            <div class="card">
                                <div class="card-header">
                                    Contacts
                                </div>
                                @foreach ($employee->phones as $phone)
                                <div class="card-body">
                                    <p>
                                        <i class="fa fa-phone" aria-hidden="true"></i> 
                                        {{ $phone->phone }}
                                    </p>
                                </div>    
                                @endforeach
                                
                            </div>
                        </div>

                        <div class="row">
                            <div class="card">
                                <div class="card-header">
                                    Whatsapp Contacts
                                </div>
                                @foreach ($employee->whatsappPhones as $whatsappPhone)
                                <div class="card-body">
                                    <p>
                                        <i class="fa fa-whatsapp" aria-hidden="true"></i>
                                        {{ $whatsappPhone->phone }}
                                    </p>
                                </div>    
                                @endforeach
                                
                            </div>
                        </div>
        
                        <div class="row">
                            <div class="card">
                                <div class="card-header">
                                    Emails
                                </div>
                                @foreach ($employee->emails as $email)
                                <div class="card-body">
                                    <p><i class="fa fa-envelope" aria-hidden="true"></i> {{ $email->email }}</p>
                                </div>    
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>        
        </div>
        @endforeach
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>
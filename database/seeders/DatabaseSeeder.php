<?php

namespace Database\Seeders;

use App\Models\Address;
use App\Models\Email;
use App\Models\Employee;
use App\Models\Phone;
use App\Models\WhatsappPhone;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        Employee::factory(10)->times(10)->create()
            ->each(function($employee){
                $employee->addresses()
                        ->saveMany(Address::factory()->times(rand(1, 3))->make());
                $employee->emails()
                        ->saveMany(Email::factory()->times(rand(1, 3))->make());
                $employee->phones()
                        ->saveMany(Phone::factory()->times(rand(1, 3))->make());
                $employee->whatsappPhones()
                        ->saveMany(WhatsappPhone::factory()->times(rand(1, 3))->make());
            });
    }
}

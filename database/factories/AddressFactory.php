<?php

namespace Database\Factories;

use App\Models\Address;
use Illuminate\Database\Eloquent\Factories\Factory;

class AddressFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Address::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'address' => $this->faker->address,
            'zip'=> rand(10000, 100000),
            'suburb' => $this->faker->text(20),
            'location' => $this->faker->text(20),
            'taluka' => $this->faker->text(20),
            'city' => $this->faker->city,
            'district' => $this->faker->text(20),
            'state' => $this->faker->state,
            'country' => $this->faker->country,
        ];
    }
}

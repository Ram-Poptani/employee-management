<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    use HasFactory;

    protected $fillable = [
        'employee_id',
        'address',
        'zip',
        'suburb',
        'location',
        'taluka',
        'city',
        'district',
        'state',
        'country',
    ];

    public function employee() {
        return $this->belongsTo(Employee::class, 'employee_id');
    }


    
}

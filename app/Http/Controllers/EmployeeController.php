<?php

namespace App\Http\Controllers;

use App\Models\Address;
use App\Models\Email;
use App\Models\Employee;
use App\Models\Phone;
use App\Models\WhatsappPhone;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    
    public function index()
    {
        $employees = Employee::all();
        return view('employee.index', compact([
            'employees'
        ]));
    }

    public function create()
    {
        return view('employee.create');
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $employee = Employee::create([
            'name' => $request->name,
        ]);
        // dd($employee);

        $addresses = [];

        for ($i=0; $i < sizeof($request->address); $i++) { 
            $addresses[$i] = Address::create([
                'employee_id' => $employee->id,
                'address' => $request->address[$i] ? $request->address[$i] : '',
                'zip' => $request->zip[$i] ? $request->zip[$i] : '',
                'suburb' => $request->suburb[$i] ? $request->suburb[$i] : '',
                'location' => $request->location[$i] ? $request->location[$i] : '',
                'taluka' => $request->taluka[$i] ? $request->taluka[$i] : '',
                'city' => $request->city[$i] ? $request->city[$i] : '',
                'district' => $request->district[$i] ? $request->district[$i] : '',
                'state' => $request->state[$i] ? $request->state[$i] : '',
                'country' => $request->country[$i] ? $request->country[$i] : '',
            ]);
        }

        // dd($employee, $addresses);
        
        $phones = [];
        for ($i=0; $i < sizeof($request->phone); $i++) { 
            // if ($request->isPrimary == $request->phone[$i]) {
            //     dd('hello', $i);
            // }else{
            //     dd('lelo', $i);
            // }
            $phones[$i] = Phone::create([
                'employee_id' => $employee->id,
                'phone' => $request->phone[$i],
                'isPrimary' => $request->isPrimary == $request->phone[$i],
            ]);
        }

        
        // dd($employee, $addresses, $phones);
        $whatsappPhones = [];
        for ($i=0; $i < sizeof($request->whatsappPhone); $i++) { 
            $whatsappPhones[$i] = WhatsappPhone::create([
                'employee_id' => $employee->id,
                'phone' => $request->whatsappPhone[$i],
                'isPrimary' => $request->isPrimaryWhatsapp == $request->whatsappPhone[$i],
            ]);
        }

        // dd($employee, $addresses, $phones, $whatsappPhones);
        $emails = [];
        for ($i=0; $i < sizeof($request->email); $i++) { 
            $emails[$i] = Email::create([
                'employee_id' => $employee->id,
                'email' => $request->email[$i],
            ]);
        }

        

        // dd($employee, $addresses, $phones, $whatsappPhones, $emails);

        return redirect()->route('employee.index');
    }

    public function destroy(Employee $employee)
    {
        $employee->forceDelete();
        return redirect()->route('employee.index');
    }

    public function edit(Employee $employee)
    {
        return view('employee.edit', compact(['employee']));
    }

    public function update(Request $request, Employee $employee)
    {

        $employee->update([
            'name' => $request->name,
        ]);
        
        
        $employee->addresses()->forceDelete();
        $addresses = [];
        for ($i=0; $i < sizeof($request->address); $i++) { 
            $addresses[$i] = Address::create([
                'employee_id' => $employee->id,
                'address' => $request->address[$i] ? $request->address[$i] : '',
                'zip' => $request->zip[$i] ? $request->zip[$i] : '',
                'suburb' => $request->suburb[$i] ? $request->suburb[$i] : '',
                'location' => $request->location[$i] ? $request->location[$i] : '',
                'taluka' => $request->taluka[$i] ? $request->taluka[$i] : '',
                'city' => $request->city[$i] ? $request->city[$i] : '',
                'district' => $request->district[$i] ? $request->district[$i] : '',
                'state' => $request->state[$i] ? $request->state[$i] : '',
                'country' => $request->country[$i] ? $request->country[$i] : '',
            ]);
        }


        $employee->phones()->forceDelete();
        $phones = [];
        for ($i=0; $i < sizeof($request->phone); $i++) { 
            $phones[$i] = Phone::create([
                'employee_id' => $employee->id,
                'phone' => $request->phone[$i],
                'isPrimary' => $request->isPrimary == $request->phone[$i],
            ]);
        }


        $employee->whatsappPhones()->forceDelete();
        $whatsappPhones = [];
        for ($i=0; $i < sizeof($request->whatsappPhone); $i++) { 
            $whatsappPhones[$i] = WhatsappPhone::create([
                'employee_id' => $employee->id,
                'phone' => $request->whatsappPhone[$i],
                'isPrimary' => $request->isPrimaryWhatsapp == $request->whatsappPhone[$i],
            ]);
        }


        $employee->emails()->forceDelete();
        $emails = [];
        for ($i=0; $i < sizeof($request->email); $i++) { 
            $emails[$i] = Email::create([
                'employee_id' => $employee->id,
                'email' => $request->email[$i],
            ]);
        }

        return redirect()->route('employee.index');
    }

}
